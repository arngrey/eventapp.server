#!/bin/bash

consul agent \
  -server `# Start consul agent in Server mode` \
  -bind $PRIVATE_IP_ADDRESS `# The address that should be bound to for internal cluster communications.` \
  -advertise $PRIVATE_IP_ADDRESS `# The advertise address is used to change the address that we advertise to other nodes in the cluster.` \
  -node $NODE `# The name of this node in the cluster. This must be unique within the cluster. By default this is the hostname of the machine.` \
  -client 0.0.0.0 `# The address to which Consul will bind client interfaces, including the HTTP, HTTPS, gRPC and DNS servers.` \
  -dns-port 53 `# The DNS port to listen on. This overrides the default port 8600.` \
  -data-dir /data `# This flag provides a data directory for the agent to store state.` \
  -ui `# Enables the built-in web UI server and the required HTTP routes.` \
  -bootstrap `# This flag is used to control if a server is in "bootstrap" mode.
  # Technically, a server in bootstrap mode is allowed to self-elect as the Raft leader.
  # It is important that only a single node is in this mode`