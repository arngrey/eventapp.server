using EventApp.Models;
using EventApp.InterfaceAdapters.DataStorage;
using EventApp.InterfaceAdapters.RestApi.Utils;
using EventApp.UseCases;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Reflection;
using Microsoft.Extensions.Logging;

namespace EventApp.InterfaceAdapters.RestApi
{
    public class Startup
    {
        public string _webServerOriginOnlyPolicy = "_webServerOriginOnlyPolicy";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(cfg => cfg.AddConsole());
            services.AddCors(options =>
            {
                options.AddPolicy(name: _webServerOriginOnlyPolicy, builder =>
                {
                    builder.WithOrigins("http://web");
                });
            });

            services.AddAutoMapper(cfg =>
            {
                cfg.AddMaps(Assembly.GetExecutingAssembly());
            });

            var session = SessionFactoryCreator.OpenSessionWithRetries();
            services.AddSingleton(session);

            services.AddSingleton<IRepository<Message>, NHibernateRepository<Message>>();
            services.AddSingleton<IRepository<User>, NHibernateRepository<User>>();
            services.AddSingleton<IRepository<Hobby>, NHibernateRepository<Hobby>>();
            services.AddSingleton<IRepository<Campaign>, NHibernateRepository<Campaign>>();

            services.AddSingleton<UserService>();
            services.AddSingleton<HobbyService>();
            services.AddSingleton<CampaignService>();

            services.AddAuthentication("BasicAuthentication")
                .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);

            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseMiddleware<RequestLoggingMiddleware>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(_webServerOriginOnlyPolicy);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
