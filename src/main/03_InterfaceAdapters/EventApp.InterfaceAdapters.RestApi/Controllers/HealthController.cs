﻿using AutoMapper;
using EventApp.InterfaceAdapters.RestApi.Dtos;
using EventApp.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace EventApp.InterfaceAdapters.RestApi.Controllers
{
    [AllowAnonymous]
    [Route("/api/health")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        public HealthController()
        {
        }

        [AllowAnonymous]
        [Route("check")]
        [HttpGet]
        public async Task<IActionResult> HealthCheck()
        {
            return Ok();
        }
    }
}