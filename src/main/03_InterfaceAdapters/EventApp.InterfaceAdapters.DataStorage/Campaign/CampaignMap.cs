﻿using EventApp.Models;
using FluentNHibernate.Mapping;

namespace EventApp.InterfaceAdapters.DataStorage
{
    /// <summary>
    /// Сопоставление полей DTO и полей сущности кампания.
    /// </summary>
    public class CampaignMap: SubclassMap<Campaign>
    {
        public CampaignMap()
        {
            Map(x => x.Name).Not.Nullable();
            HasManyToMany(x => x.Hobbies)
                .Table("CampaignHobby");
            References(x => x.Administrator).Not.Nullable();
            HasManyToMany(x => x.Participants)
                .Table("CampaignParticipant");
            HasMany(x => x.Messages);
        }
    }
}
