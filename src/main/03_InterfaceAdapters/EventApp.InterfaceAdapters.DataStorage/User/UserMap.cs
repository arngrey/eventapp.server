﻿using FluentNHibernate.Mapping;
using EventApp.Models;

namespace EventApp.InterfaceAdapters.DataStorage
{
    /// <summary>
    /// Сопоставление полей DTO и полей сущности пользователь.
    /// </summary>
    public class UserMap: SubclassMap<User>
    {
        public UserMap()
        {
            Map(x => x.Login).Not.Nullable();
            Map(x => x.Password).Not.Nullable();
            Map(x => x.FirstName).Nullable();
        }
    }
}
